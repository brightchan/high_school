# Map measuring utility #

The repository contains the source code of a Delphi project for the Youth Technology Innovation Competition (2006) in my high school. The compiled exe utilizes a webcam to measure the distant or area specified by the route of a laser pointer on a map or other flat surface.

The project is a joint work of us three students: Haipeng Yu, Jianbin Chen and Diheng Zhang.