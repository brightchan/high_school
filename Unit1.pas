# -*- coding: utf-8 -*-

unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, SUISideChannel, SUIMgr, SUIComboBox,
  SUIMemo, SUIEdit, SUIButton, SUIForm, Menus, ComCtrls;
type
  TFormMain = class(TForm)
    Panel1: TPanel;
    LabLoad: TLabel;
    ButtAnlayse: TButton;
    ButtPhoto: TButton;
    ButtStopP: TButton;
    Edit1: TEdit;
    ComDW: TComboBox;
    MainMenu: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    Edit2: TEdit;
    ButtS: TButton;
    ComS: TComboBox;
    Label1: TLabel;
    N16: TMenuItem;
    N17: TMenuItem;
    mainsave: TSaveDialog;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    StatusBar1: TStatusBar;
    N21: TMenuItem;

   // procedure suitempButtfilmClick(Sender: TObject);
    procedure suitempButtPhotoClick(Sender: TObject);
    procedure suitempButtStopPClick(Sender: TObject);
    procedure suitempButtAnlayseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
   // procedure suitempButtTextClick(Sender: TObject);
    procedure suitempButtHelpClick(Sender: TObject);
   //procedure suitempComDWSelect(Sender: TObject);
  //  procedure ButtColTexClick(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure ButtSClick(Sender: TObject);
    procedure ComSChange(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    //procedure N13Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure ComDWChange(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure DoShowHint(Sender: TObject);
    procedure N21Click(Sender: TObject);

  private
    { Private declarations }
  public
{ Public declarations }
  end;

var
  FormMain: TFormMain;
  load:double;
  rule2:integer;
  hWndC : THandle;
  jie,catching,flag: boolean;
  rule3,rule,Rm,Rn,Gm,Gn,Bm,Bn,precision1,precision:integer;
  BitPic:TBitmap;
  k:longint;
  s:real;
  march:array[1..10000,1..3]of integer;
  co:array[0..1000]of integer;
  const WM_CAP_START = WM_USER;
  const WM_CAP_STOP = WM_CAP_START + 68;
  const WM_CAP_DRIVER_CONNECT = WM_CAP_START + 10;
  const WM_CAP_DRIVER_DISCONNECT = WM_CAP_START + 11;
  const WM_CAP_SAVEDIB = WM_CAP_START + 25;
  const WM_CAP_GRAB_FRAME = WM_CAP_START + 60;
  const WM_CAP_SEQUENCE = WM_CAP_START + 62;
  const WM_CAP_FILE_SET_CAPTURE_FILEA = WM_CAP_START + 20;
  const WM_CAP_SEQUENCE_NOFILE =WM_CAP_START+63;
  const WM_CAP_SET_OVERLAY =WM_CAP_START+  51;
  const WM_CAP_SET_PREVIEW =WM_CAP_START+  50;
  const WM_CAP_SET_CALLBACK_VIDEOSTREAM = WM_CAP_START +6;
  const WM_CAP_SET_CALLBACK_ERROR=WM_CAP_START +2;
  const WM_CAP_SET_CALLBACK_STATUSA= WM_CAP_START +3;
  const WM_CAP_SET_CALLBACK_FRAME= WM_CAP_START +5;
  const WM_CAP_SET_SCALE=WM_CAP_START+  53;
  const WM_CAP_SET_PREVIEWRATE=WM_CAP_START+ 52 ;


function capCreateCaptureWindowA(lpszWindowName:PCHAR; dwStyle:longint;
  x:integer; y:integer;nWidth : integer; nHeight : integer;
  ParentWin : HWND;nId : integer): HWND;
STDCALL EXTERNAL 'AVICAP32.DLL';
procedure ButtStartClick;
procedure printt;
procedure Delay(MSecs: Longint);   //*****��ʱ������MSecs��λΪ����(ǧ��֮1��)

implementation

uses unit2,unit3,unit4,unit5,unit6,unit7, ViewWin, Unit8;
{$R *.dfm}


procedure Delay(MSecs: Longint);   //*****��ʱ������MSecs��λΪ����(ǧ��֮1��)
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount();
  repeat
    Application.ProcessMessages;
    Now := GetTickCount();
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

//*****��ɫֵ������
{procedure TFormMain.suitempEdit2Change(Sender: TObject);
begin
  Rm:=strtoint(formcolset.edit2.text);
end;
procedure TFormMain.suitempEdit3Change(Sender: TObject);
begin
 Gm:=strtoint(formcolset.edit3.text);
end;
procedure TFormMain.suitempEdit4Change(Sender: TObject);
begin
 Bm:=strtoint(formcolset.edit4.text);
end;
procedure TFormMain.suitempEdit5Change(Sender: TObject);
begin
  Rn:=strtoint(formcolset.edit5.text);
end;
procedure TFormMain.suitempEdit6Change(Sender: TObject);
begin
  Gn:=strtoint(formcolset.edit6.text);
end;
procedure TFormMain.suitempEdit7Change(Sender: TObject);
begin
  Bn:=strtoint(formcolset.edit7.text);
end;
//������� }

procedure ButtStartClick; //*****��������ͷ
begin
  Rm:=$ff;Rn:=$F0;Gm:=$FF;Gn:=$F0;Bm:=$FF;Bn:=$F0;
  hWndC := capCreateCaptureWindowA('My Own Capture Window',WS_CHILD or WS_VISIBLE ,formmain.Panel1.Left,formmain.Panel1.Top,formmain.Panel1.Width,formmain.Panel1.Height,FormMain.Handle,0);
  if hWndC <> 0 then
      begin
         SendMessage(hWndC, WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0, 0);
         SendMessage(hWndC, WM_CAP_SET_CALLBACK_ERROR, 0, 0);
         SendMessage(hWndC, WM_CAP_SET_CALLBACK_STATUSA, 0, 0);
         SendMessage(hWndC, WM_CAP_DRIVER_CONNECT, 0, 0);
         SendMessage(hWndC, WM_CAP_SET_SCALE, 1, 0);
         SendMessage(hWndC, WM_CAP_SET_PREVIEWRATE, 66, 0);
         SendMessage(hWndC, WM_CAP_SET_OVERLAY, 1, 0);
         SendMessage(hWndC, WM_CAP_SET_PREVIEW, 1, 0);
      end;
   catching:=false;
end;



{procedure TFormMain.suitempButtfilmClick(Sender: TObject);//*****����¼����
begin
  if hWndC<>0 then
    begin
      if not(catching) then
          begin
           Buttfilm.Caption:='ֹͣ¼��';
           SendMessage(hWndC,WM_CAP_FILE_SET_CAPTURE_FILEA,0, Longint(pchar('film.avi')));
           SendMessage(hWndC, WM_CAP_SEQUENCE, 0, 0);
           catching:=true;
          end
       else begin
              Buttfilm.Caption:='¼    ��';
              catching:=false;
              SendMessage(hWndC, WM_CAP_STOP, 0, 0);
              SendMessage(hWndC, WM_CAP_STOP, 0, 0);
            end;
    end;
end; }


procedure TFormMain.suitempButtPhotoClick(Sender: TObject);//*****��ȡ����˶��켣��������һ�����̣�
begin
   formmain.Hint:='����ͷ��ȡ��Ϣ�С�����ɺ��밴��ֹͣ��';
   ButtS.Enabled:=false;
   jie:=true;
   k:=0;
   if hWndC <> 0 then
     begin
      while jie=true do
         begin
           inc(k);
           SendMessage(hWndC,WM_CAP_SAVEDIB,0,longint(pchar('pic/'+IntToStr(k)+'.bmp')));
           delay(strtoint(formdelay.edit1.Text));
         end;
     end;
end;

procedure TFormMain.suitempButtStopPClick(Sender: TObject);
begin
  jie:=false;
  formmain.Hint:='��ȡ��Ϣ��ɣ���ѡ�������������';
end;

function calculate(n1,n2:integer): double;//*****������������
var
  w,h:integer;
begin
  w:=march[n1,1]-march[n2,1];
  h:=march[n1,2]-march[n2,2];
  calculate:=sqrt(w*w+h*h);
end;

procedure printt;//*****���������
  var
    i:integer;
  begin
    formprintt.memo1.Clear;
    formprintt.memo1.Lines.Add('��ͼ��:'+inttostr(k));
    formprintt.memo1.Lines.Add('���㼰ȡɫֵ��') ;
    for i:=1 to k do
        formprintt.memo1.Lines.Add(inttostr(i)+' : '+inttostr(march[i,1])+','+inttostr(march[i,2])+' ( '+ inttohex(co[i],6));
        formprintt.memo1.lines.add(inttostr(formshow.iMprint.Height)+','+inttostr(formshow.iMpaint.Width));
  end;

procedure TFormMain.suitempButtAnlayseClick(Sender: TObject);//*****�������켣
var
 No,r,g,b,color:integer;
 x,y:longint;
 point:boolean;
 fn:string;


begin//  *********������******
  formmain.Hint:='���ݴ����С������Ժ�';
  load:=0;
  fillchar(march,sizeof(march),0);
  fillchar(co,sizeof(co),0);
  BitPic:=TBitMap.Create;
  for no:=1 to k do
     begin
       fn:='D:\���\�������Final4.60\pic\'+IntToStr(no)+'.bmp';
       BitPic.LoadFromFile(fn); //���뱻�����ļ�
       point:=false;
       for y:=0 to BitPic.Height-1 do
         if not(point) then
           begin
            for x:=0 to Bitpic.Width-1 do
              begin
               r:=GETRVALUE(BitPic.Canvas.Pixels[x,y]);
               g:=GETGVALUE(BitPic.Canvas.Pixels[x,y]);
               b:=GETBVALUE(BitPic.Canvas.Pixels[x,y]);
               color:=b+g*$100+r*$10000;
               if (r<=rm) and (r>=rn) and(b<=bm) and (b>=bn) and (g>=gn) and (g<=gm) then //ȡ��
                   begin
                     march[no,1]:=x;
                     march[no,2]:=y;
                     co[no]:=color;
                     point:=true;
                     break;
                   end;
              end;
           end
         else break;
     end;
  formshow.Imprint.Picture.LoadFromFile(fn);
  BitPic.Destroy;
  formshow.iMpaint.Canvas.Brush.Style:=bsSolid;
  formshow.iMpaint.Canvas.Pen.Style:=psSolid;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Rectangle(0,0,formshow.iMpaint.Width,formshow.iMpaint.Height);

  for x:=1 to k do if (march[x,1]<>0)and(march[x,2]<>0) then break;
  y:=x+1;
  while y<=k do
    if (calculate(x,y)>=1) and (march[y,1]<>0)and(march[y,2]<>0) then
         begin
           formshow.iMpaint.Canvas.Pen.Color:=clred;
           formshow.iMpaint.Canvas.MoveTo(march[x,1],march[x,2]);
           formshow.iMpaint.Canvas.LineTo(march[y,1],march[y,2]);
           formshow.iMprint.Canvas.MoveTo(march[x,1],march[x,2]);
           formshow.iMprint.Canvas.LineTo(march[y,1],march[y,2]);
           load:=load+calculate(x,y);
           x:=y;
           inc(y);
         end
     else inc(y);
     FormMain.ComDWChange(Self);
     printt;
     ButtS.Enabled:=True;
     //formmain.Hint:='������ɣ�';
     //delay(10000);
     formmain.Hint:='������ɣ�����״̬����������ͷ�����С���';
end;
//������ť������


procedure TFormMain.FormCreate(Sender: TObject); //****���������
begin
  formmain.Show;
  formmain.Hint:='��ӭʹ�����ܵ�ͼ����ǣ����ȳ�ʼ������ʹ��';
  application.ShowHint:=true;
  application.OnHint:=doshowhint;//ָ����ʾϵͳHint�Ĺ���
  load:=0;
  rule:=1;
  rule2:=1;
  rule3:=1;
  //application.messagebox('ʹ��ǰ�����Ķ�ʹ��˵����','��ܰ��ʾ');
  flag:=true;
  formmain.Hint:='��ӭʹ�����ܵ�ͼ����ǣ����ȳ�ʼ������ʹ��';
end;
procedure TFormmain.DoShowHint(Sender: TObject);
begin
  formmain.StatusBar1.Panels[0].Text:=Application.Hint;//��ʾӦ�ó������ʾ
end;


{procedure TFormMain.suitempButtTextClick(Sender: TObject);//*****��ʼ��

    begin //*****��ʼ����ť
       if flag then begin FormMain.ButtStartClick(Self); flag:=false; end;
         case application.messagebox('�뽫������뱻�ⷶΧ������OK���ƶ�һ����֪����,�����¡�ֹͣ��ȡ��','��ʼ��')of
     idok:begin
            suitempButtPhotoClick(Self);
            case application.messagebox('�ƶ�����,���ٴΰ���ok','��ʼ��')of
              idok:begin
                     suitempButtAnlayseClick(self);
                     formrule.Show;
                   end;
            end;
          end;
     end;
end;}

procedure TFormMain.suitempButtHelpClick(Sender: TObject); //*****������ť
begin
  application.messagebox('ÿ�������������г����ʼ������ʼ�����ٰ��¡�����������ԭ�������ݱȽ���������ʼ���ɹ�������ʹ�á�','ʹ��˵��');
end;



{procedure TFormMain.ButtColTexClick(Sender: TObject);

procedure fix;
  var
   max,No,r,g,b,color:integer;
   x,y,precision1:longint;
   sum:extended;
   fn,temp:string;
   co:array[1..10000]of integer;

  begin
  fillchar(co,sizeof(co),0);
  BitPic:=TBitMap.Create;
  for no:=1 to k do
     begin
       fn:='pic/'+IntToStr(no)+'.bmp';
       BitPic.LoadFromFile(fn); //���뱻�����ļ�
       max:=0;
       for y:=0 to BitPic.Height  do
           begin
            for x:=0 to Bitpic.Width do
               begin
               r:=BitPic.Canvas.Pixels[x,y] mod $100;
               g:=BitPic.Canvas.Pixels[x,y] mod $10000-r;
               b:=(BitPic.Canvas.Pixels[x,y]-r-g) div $10000;
               color:=b+g+r*$10000;
               if max<color then max:=color;
               end;
           end;
       co[no]:=max;
     end;
     sum:=0;
  for no:=1 to k do sum:=sum+co[no];
  precision:=rn+bn*$100+gn*$10000;
  precision1:=round(sum/k);
  precision1:=round((precision1+precision)/2);
  temp:=inttohex(precision1,6);
  formcolset.edit5.text:='$'+copy(temp,1,2);
  formcolset.edit6.text:='$'+copy(temp,3,2);
  formcolset.edit7.text:='$'+copy(temp,5,2);
end;
    begin //*****��ʼ����ť
      if flag then begin FormMain.ButtStartClick(Self); flag:=false; end;
         case application.messagebox('�뽫������뱻�ⷶΧ������OK���ƶ�һ�ξ���,�����¡�ֹͣ��ȡ��','��ɫֵ��ʼ��')of
     idok:begin
            suitempButtPhotoClick(Self);
            case application.messagebox('�ƶ�����,���ٴΰ���ok','��ɫֵ��ʼ��')of
              idok:fix;
            end;
           end;
     end;
     end; }



{procedure TFormMain.suitempEdit8Change(Sender: TObject);
begin
  rule:=strtoint(edit8.text);
end; }





procedure TFormMain.ButtSClick(Sender: TObject);//********�����
{var
  i,j,x,y,x1,x2,y1,y2:integer;
begin
  x:=march[1,1];
  y:=march[1,2];
  s:=0;
  for i:=2 to k do
    begin
      x2:=march[i,1]-x;
      y2:=march[i,2]-y;
      x1:=march[i-1,1]-x;
      y1:=march[i-1,2]-y;
      s:=s+abs(x1*y2-x2*y1);
    end;
  FormMain.ComSChange(Self);
  formmain.Hint:='������ɣ�';
  delay(1000);
  formmain.Hint:='������ɣ�����״̬����������ͷ�����С���';
end;}
const dot=76800;
var
 i,ssum,lines,count:longint;
 a:array[1..dot,1..2] of word;

procedure sort(k,x,y:integer); //��ȱ�ǳ���   line=1:��չ�� line=2:�ѱ�� line=4:�ѱ������ line=-1δ�������:
begin
   if (linef[x,y]) then
    begin
    if line[x,y]=1 then
     begin
      line[x,y]:=2; inc(ssum);//����Ѷ�ȡ��,��¼���
      begin
        if (line[x+1,y]mod 2<>0) and (line[x+1,y]<>1) and (linef[x+1,y]) and (x<Formshow.Impaint.Picture.Width-1) then
         begin inc(count); a[count,1]:=x+1; a[count,2]:=y; line[x+1,y]:=1;  end;
        if (line[x-1,y]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (x>0) then
         begin inc(count); a[count,1]:=x-1; a[count,2]:=y; line[x-1,y]:=1; end;
        if (line[x,y+1]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (x<Formshow.Impaint.Picture.Height-1) then
         begin inc(count); a[count,2]:=y+1; a[count,1]:=x; line[x,y+1]:=1; end;
        if (line[x,y-1]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (y>0) then
         begin inc(count); a[count,2]:=y-1; a[count,1]:=x; line[x,y-1]:=1; end;
      end;
     end
    end
   //else begin line[x,y]:=4;  end;
  end;


procedure analyse;
 var
 r,g,b:integer;
 x,y:integer;
 begin
   for y:=0 to Formshow.Impaint.Picture.Height-1 do
       begin
        for x:=0 to Formshow.Impaint.Picture.Width-1 do
          begin
           r:=GETRVALUE(Formshow.Impaint.Canvas.Pixels[x,y]);
           g:=GETGVALUE(Formshow.Impaint.Canvas.Pixels[x,y]);
           b:=GETBVALUE(Formshow.Impaint.Canvas.Pixels[x,y]);
           if (r<=255) and (r>=100) and(b<=0) and (b>=0) and (g>=0) and (g<=0) then //ȡ��
            begin linef[x,y]:=false; inc(lines); end;
          end;
       end
 end;

begin
 fillchar(line,sizeof(line),3);
 fillchar(linef,sizeof(linef),true);
 ssum:=0;
 lines:=0;
 analyse;
 fillchar(a,sizeof(a),0);
 a[1,1]:=0; a[1,2]:=0; line[0,0]:=1; count:=1;
 for i:=1 to dot do
  sort(i,a[i,1],a[i,2]);
 S:=76800-ssum+lines;
 FormMain.ComSChange(Self);
  //formmain.Hint:='������ɣ�';
  //delay(1000);
  formmain.Hint:='������ɣ�����״̬����������ͷ�����С���'
end;//*****�������

procedure TFormMain.ComDWChange(Sender: TObject);//*****·�̵Ľ���ת��
var
  num2:string;
  t,t1:integer;
begin
     t1:=0;
     if comdw.Text  ='cm' then
        edit1.text:=inttostr(round(load*rule/rule2))+'.'+inttostr(round(load*rule/rule2*10)mod 10)
     else
     begin if comdw.text ='m' then
        begin
          rule3:=100;
          t1:=2;
        end;
      if comdw.Text ='km' then
        begin
          rule3:=100000;
          t1:=5;
        end;
        num2:=inttostr(round(load*rule/rule2)mod rule3);
        t:=length(num2);
        if t1<>0 then
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        edit1.text:=inttostr(round(load*rule/rule2)div rule3)+'.'+num2;
     end;
end;


procedure TFormMain.ComSChange(Sender: TObject);//*****����Ľ���ת��
{var
  num2:string;
  t,t1:integer;
begin
     if comS.Text  ='cm^2' then
        edit2.text:=inttostr(round(S*(rule*rule/rule2/rule2)))
     else
     begin if comS.text ='m^2' then
        begin
          rule3:=10000;
          t1:=4;
        end;
      if comS.Text ='km^2' then
        begin
          rule3:=100000;
          t1:=10;
        end;
        if t1=10 then
        num2:=inttostr((round(s*(rule*rule/rule2/rule2))mod rule3) mod 100000)
        else num2:=inttostr(round(s*(rule*rule/rule2/rule2))mod rule3);
        t:=length(num2);
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        if t1=10 then
        edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3 div 100000)+'.'+num2
        else edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3)+'.'+num2
     end;}
var
  num2:string;
  t,t1:integer;
begin
     if comS.Text  ='cm^2' then
        edit2.text:=inttostr(round(S*(rule*rule/rule2/rule2)))
     else begin if comS.text ='m^2' then
        begin
          rule3:=10000;
          t1:=4;
        end;
      if comS.Text ='km^2' then
        begin
          rule3:=100000;
          t1:=10;
        end;
        if t1=10 then
        num2:=inttostr((round(s*(rule*rule/rule2/rule2))mod rule3) mod 100000)
        else num2:=inttostr(round(s*(rule*rule/rule2/rule2))mod rule3);
        t:=length(num2);
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        if t1=10 then
        edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3 div 100000)+'.'+num2
        else edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3)+'.'+num2
    end;
end;





procedure TFormMain.N10Click(Sender: TObject);//��ʼ��
var x,y:integer;
begin
  formmain.Hint:='�����ʼ����!�밴��ʾ���в�������';
  load:=0;
  k:=0;
  FormColSet.ButtColTexClick(Self);
  for x:=1 to k do if (march[x,1]<>0)and(march[x,2]<>0) then break;
  y:=x+1;
  formshow.iMpaint.Canvas.Brush.Style:=bsSolid;
  formshow.iMpaint.Canvas.Pen.Style:=psSolid;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Rectangle(0,0,formshow.iMpaint.Width,formshow.iMpaint.Height);
  while y<=k do
    if (calculate(x,y)>=1) and (march[y,1]<>0)and(march[y,2]<>0) then
         begin
           formshow.iMpaint.Canvas.Pen.Color:=clred;
           formshow.iMpaint.Canvas.MoveTo(march[x,1],march[x,2]);
           formshow.iMpaint.Canvas.LineTo(march[y,1],march[y,2]);
           load:=load+calculate(x,y);
           x:=y;
           inc(y);
         end
     else inc(y);

  if load<>0 then Formrule.ShowModal
             else begin
                    showmessage('û�м�⵽����㣡�����³�ʼ����');
                    formmain.Hint:='��ʼ��ʧ�ܣ������³�ʼ������ʹ��';
                  end;
  Formmain.ComDWChange(Self);
  printt;
end;

procedure TFormMain.N14Click(Sender: TObject);
begin
  rule:=1;
  rule2:=1;
  rule3:=1;
  FormColSet.edit3.text:='$00';
  FormColSet.edit4.text:='$00';
  FormColSet.edit6.text:='$00';
  FormColSet.edit7.text:='$00';
  FormColSet.edit2.text:='$FF';
  FormColSet.edit5.text:='$F0';
  Rm:=strtoint(FormColSet.edit2.text);
  rn:=strtoint(FormColSet.edit5.text);
  Gm:=strtoint(FormColSet.edit3.text);
  Bm:=strtoint(FormColSet.edit4.text);
  Gn:=strtoint(FormColSet.edit6.text);
  Bn:=strtoint(FormColSet.edit7.text);
  formmain.Hide;
  formcolset.ButtcolTex.Enabled:=false;
  formdisset.ButtText.Enabled:=false;
  formpic.show;
  Application.OnHint:=formpic.DoShowHint2;//ָ����ʾϵͳHint�Ĺ���
end;

{procedure TFormMain.N13Click(Sender: TObject);
begin
  formmain.Show;
  formdisset.ButtText.Enabled:=true;
  formcolset.ButtcolTex.Enabled:=true;
  formpic.hide;
end;}
 procedure TFormMain.N15Click(Sender: TObject);
begin
   formmain.Close;
end;


procedure TFormMain.N9Click(Sender: TObject);
begin
   if n9.checked=false then
  begin
   formprintt.show ; n9.Checked:=true;
  end
 else begin
   formprintt.hide ; n9.Checked:=false;
  end
end;

procedure TFormMain.N8Click(Sender: TObject);
begin
 if n8.checked=false then
  begin
   formshow.show ; n8.Checked:=true;
  end
 else begin
   formshow.hide ; n8.Checked:=false;
  end
end;

procedure TFormMain.N12Click(Sender: TObject);
begin
  formdisset.show;
  enablewindow(formPic.handle,false);
  enablewindow(formmain.handle,false);//���岻����
end;

procedure TFormMain.N11Click(Sender: TObject);
begin
  formcolset.Show;
  enablewindow(formPic.handle,false);
  enablewindow(formmain.handle,false);//���岻����
end;


procedure TFormMain.N17Click(Sender: TObject);
var
  pathname:string;
begin
  mainsave.FileName:='�ޱ���';
  if mainsave.Execute then
    begin
      pathname:=mainsave.FileName;
      if mainsave.Filterindex=1 then
          pathname:=pathname+'.bmp';
      formshow.impaint.Picture.SaveToFile(pathname);
end;
end;

procedure TFormMain.N16Click(Sender: TObject);
var
  pathname:string;
begin
    mainsave.FileName:='�ޱ���';
  if mainsave.Execute then
    begin
      pathname:=mainsave.FileName;
      if mainsave.Filterindex=1 then
          pathname:=pathname+'.bmp';
      formshow.imprint.Picture.SaveToFile(pathname);
end;
end;


procedure TFormMain.N7Click(Sender: TObject);
begin
  formabout.showModal;
end;

procedure TFormMain.N21Click(Sender: TObject);
begin
 formdelay.showmodal;
end;

end.

