unit Unit7;

interface 

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, ExtCtrls, ComCtrls;

type
  TFormPic = class(TForm)
    MainMenu: TMainMenu;
    N1: TMenuItem;
    N15: TMenuItem;
    N2: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N3: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N4: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    LabLoad: TLabel;
    Label1: TLabel;
    Edit1: TEdit;
    ComDW: TComboBox;
    Edit2: TEdit;
    ComS: TComboBox;
    image1: TImage;
    N16: TMenuItem;
    ButtAnlayse: TButton;
    ButtS: TButton;

    ButtPhoto: TButton;
    ButtStopP: TButton;
    ButtSta: TButton;
    picSave: TSaveDialog;
    N17: TMenuItem;
    N18: TMenuItem;
    N10: TMenuItem;
    N19: TMenuItem;
    StatusBar1: TStatusBar;
    N20: TMenuItem;
    ColorDialog1: TColorDialog;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    OpenDialog1: TOpenDialog;
    procedure N15Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    //procedure N14Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
    procedure image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
    procedure image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
    procedure ButtAnlayseClick(Sender: TObject);
    procedure ButtSClick(Sender: TObject);
    procedure ButtPhotoClick(Sender: TObject);
    procedure ButtStopPClick(Sender: TObject);
    procedure ButtStaClick(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure DoShowHint2(Sender: TObject);
    procedure ComDWChange(Sender: TObject);
    procedure ComSChange(Sender: TObject);
    //procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N7Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
   // procedure N3Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPic: TFormPic;
  pathnamett:string;
  BTT,StaD,staS,showcol,restart:boolean;   //restart(初始化）
  line:array [0..320,0..240] of shortint;
  linef:array[0..320,0..240] of boolean;

implementation

uses Unit5, Unit6, Unit1, Unit3, Unit4,imagewin, ViewWin, Unit2;

{$R *.dfm}


{procedure TFormpic.N14Click(Sender: TObject);
begin
  formmain.Hide;
  formpic.show;
  Application.OnHint:=formpic.DoShowHint2;//指定显示系统Hint的过程
end;}

procedure TFormpic.N13Click(Sender: TObject);
begin
  formmain.show;
  formpic.hide;
  formcolset.ButtcolTex.Enabled:=true;
  formdisset.ButtText.Enabled:=true;
  FormColSet.edit3.text:='$FF';
  FormColSet.edit4.text:='$FF';
  FormColSet.edit6.text:='$00';
  FormColSet.edit7.text:='$00';
  Gm:=strtoint(FormColSet.edit3.text);
  Bm:=strtoint(FormColSet.edit4.text);
  Gn:=strtoint(FormColSet.edit6.text);
  Bn:=strtoint(FormColSet.edit7.text);
  application.OnHint:=formmain.doshowhint;//指定显示系统Hint的过程
end;
 procedure TFormpic.N15Click(Sender: TObject);
begin
   formmain.Close;
end;
procedure TFormpic.N12Click(Sender: TObject);
begin
  formdisset.show;
  enablewindow(formPic.handle,false);
  enablewindow(formmain.handle,false);//窗体不可用
end;

procedure TFormpic.N9Click(Sender: TObject);
begin
  if n9.checked=false then
  begin
   formprintt.show ; n9.Checked:=true;
  end
 else begin
   formprintt.hide ; n9.Checked:=false;
  end
end;

procedure TFormpic.N8Click(Sender: TObject);
begin
 if n8.checked=false then
  begin
   formshow.show ; n8.Checked:=true;
  end
 else begin
   formshow.hide ; n8.Checked:=false;
  end
end;

procedure TFormpic.N11Click(Sender: TObject);
begin
  formcolset.show;
  enablewindow(formPic.handle,false);
  enablewindow(formmain.handle,false);//窗体不可用
end;

procedure TFormPic.N16Click(Sender: TObject);

begin
  Application.messagebox('输入的图片大小请保证为320×240，否则会造成数据异常！','注意！');
  //imageform.show;
   opendialog1.FileName:='无标题';
  if opendialog1.Execute then
    begin
      pathnamett:=opendialog1.FileName;
      formpic.Image1.Picture.LoadFromFile(pathnamett);
    end;
  formpic.Hint:='请选择所需的操作方式，开始操作……';
  case application.MessageBox('请设定图片的比例尺（正确设定后方能使用）！','使用提示') of
    idok: formdisset.show;
  end;
end;

procedure TFormPic.FormCreate(Sender: TObject);
begin
  formpic.Hint:='请读入待处理的图片…';
  pathnamett:=' ';
  image1.Canvas.Brush.Style :=bsClear;
  Tag:=0;
  BTT:=false;
  staS:=true;
  staD:=false;
  formpic.image1.Canvas.Pen.Color :=RGB(255,255,255);
  ButtS.Enabled:=false;
  showcol:=false;
  restart:=false;
end;
procedure TFormpic.DoShowHint2(Sender: TObject);
begin
formpic.StatusBar1.Panels[0].Text:=Application.Hint;//显示应用程序的提示
end;

procedure TFormpic.image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Tag:=1; //置位
image1.Canvas.MoveTo(X,Y); //将Pen的位置移到当前
//取随机数
//image1.Canvas.Pen.Width := Random(30);//设置画笔的宽度
march[k,1]:=x;
march[k,2]:=y;
end;

procedure TFormpic.image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
if Tag=1 then if  BTT=true then
begin
   image1.Canvas.LineTo(x,y);//画线
   inc(k);
   march[k,1]:=x;
   march[k,2]:=y;
end;
if showcol then
   formpic.StatusBar1.Panels[0].Text:='$'+inttohex(GETBVALUE(image1.Canvas.Pixels[x,y])+GETGVALUE(image1.Canvas.Pixels[x,y])*$100+GETRVALUE(image1.Canvas.Pixels[x,y])*$10000,6);
end;

procedure TFormpic.image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Tag:=0;
if staD then
begin
march[k,3]:=1;//标志位
inc(k);
end;
end;



procedure TFormpic.ButtAnlayseClick(Sender: TObject);//*****分析光点轨迹
var
 No,r,g,b,color:integer;
 x,y:longint;
 point:boolean;
 fn:string;

function calculate(n1,n2:integer): double;//*****计算两点间距离
var
  w,h:integer;
begin
  w:=march[n1,1]-march[n2,1];
  h:=march[n1,2]-march[n2,2];
  calculate:=sqrt(w*w+h*h);
end;

procedure printt;//*****坐标点的输出
  var
    i:integer;
  begin
    formprintt.memo1.Clear;
    formprintt.memo1.Lines.Add('截图数:'+inttostr(k));
    formprintt.memo1.Lines.Add('各点及取色值：') ;
    formprintt.memo1.lines.add(inttostr(formshow.iMprint.Height)+','+inttostr(formshow.iMpaint.Width));
    for i:=1 to k do
        formprintt.memo1.Lines.Add(inttostr(i)+' : '+inttostr(march[i,1])+','+inttostr(march[i,2])+'  '+ inttohex(co[i],6));
  end;

begin//  *********主程序******
  load:=0;
  formshow.iMpaint.Canvas.Brush.Style:=bsSolid;
  formshow.iMpaint.Canvas.Pen.Style:=psSolid;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Pen.Color:=clwhite;
  formshow.iMpaint.Canvas.Rectangle(0,0,formshow.iMpaint.Width,formshow.iMpaint.Height);
  for no:=1 to k-1 do
    if (calculate(no,no+1)>=1)and (march[No,3]=0) then
         begin
           formshow.iMpaint.Canvas.Pen.Color:=clred;
           formshow.iMpaint.Canvas.MoveTo(march[no,1],march[no,2]);
           formshow.iMpaint.Canvas.LineTo(march[no+1,1],march[no+1,2]);
           load:=load+calculate(no,no+1);
         end;
    
     printt;
  if not restart then Formpic.ComDWchange(Self);
  ButtS.Enabled:=true;
  //formpic.Hint:='操作完成！';
  //delay(1000);
  formpic.Hint:='操作完成！请选择所需的操作方式，开始操作……';
end;
//分析按钮程序完


{procedure TFormpic.ButtSClick(Sender: TObject);
var
  i,j,x,y,x1,x2,y1,y2:integer;
begin
  x:=march[1,1];
  y:=march[1,2];
  s:=0;
  for i:=2 to k do
    begin
      x2:=march[i,1]-x;
      y2:=march[i,2]-y;
      x1:=march[i-1,1]-x;
      y1:=march[i-1,2]-y;
      s:=s+abs(x1*y2-x2*y1);
    end;
  Formpic.ComSchange(Self);
  formpic.Hint:='操作完成！';
  delay(10000);
  formpic.Hint:='请选择所需的操作方式，开始操作……';
end;}

procedure TFormPic.ButtPhotoClick(Sender: TObject);
begin
  ButtS.Enabled:=false;
  if pathnamett<>' ' then
  formpic.Image1.Picture.LoadFromFile(pathnamett)
  else begin
  formpic.image1.Canvas.Brush.Style:=bsSolid;
  formpic.image1.Canvas.Pen.Style:=psSolid;
  formpic.image1.Canvas.Pen.Color:=clwhite;
  formpic.image1.Canvas.Pen.Color:=clwhite;
  formpic.image1.Canvas.Rectangle(0,0,formshow.iMpaint.Width,formshow.iMpaint.Height);
  end;
  BTT:=true;
  k:=1;
  fillchar(march,sizeof(march),0);
  formpic.image1.Canvas.Pen.Color:=colordialog1.Color;
  formpic.Hint:='路线确定中……结束后请按下“停止”';
end;

procedure TFormPic.ButtStopPClick(Sender: TObject);
begin
  BTT:=false;
  formpic.Hint:='数据读取完成！请选择所需操作';
  if restart then
   begin
   Formpic.ButtAnlayseClick(self);
   formrule.ShowModal;
   restart:=false;
   end;
end;

{procedure TFormpic.ComDWSelect(Sender: TObject);//*****路程的进制转换
var
  num2:string;
  t,t1:integer;
begin
     t1:=0;
     if comdw.Text  ='cm' then
        edit1.text:=inttostr(round(load*rule/rule2))
     else begin if comdw.text ='m' then
        begin
          rule3:=100;
          t1:=2;
        end;
      if comdw.Text ='km' then
        begin
          rule3:=100000;
          t1:=5;
        end;
        num2:=inttostr(round(load*rule/rule2)mod rule3);
        t:=length(num2);
        if t1<>0 then
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        edit1.text:=inttostr(round(load*rule/rule2)div rule3)+'.'+num2;
    end;
end;

procedure TFormpic.ComSSelect(Sender: TObject);//*****面积的进制转换
var
  num2:string;
  t,t1:integer;
begin
     if comS.Text  ='cm^2' then
        edit2.text:=inttostr(round(S*(rule*rule/rule2/rule2)))
     else begin if comS.text ='m^2' then
        begin
          rule3:=10000;
          t1:=4;
        end;
      if comS.Text ='km^2' then
        begin
          rule3:=100000;
          t1:=10;
        end;
        if t1=10 then
        num2:=inttostr((round(s*(rule*rule/rule2/rule2))mod rule3) mod 100000)
        else num2:=inttostr(round(s*(rule*rule/rule2/rule2))mod rule3);
        t:=length(num2);
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        if t1=10 then
        edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3 div 100000)+'.'+num2
        else edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3)+'.'+num2
    end;
end;  }


procedure TFormPic.ButtStaClick(Sender: TObject);
begin
  if staD then
    begin
      formPic.ButtSta.Caption:='转到多线段求和状态';
      formpic.StatusBar1.Panels[1].Text:='你正在使用的是：图片处理界面(连续路程状态)';
      staD:=false;
      staS:=true;
    end
    else if staS then
           begin
             formPic.ButtSta.Caption:='转到连续路程状态';
             formpic.StatusBar1.Panels[1].Text:='你正在使用的是：图片处理界面(多线段求和状态)';
             staD:=true;
             staS:=false;
           end;
end;

procedure TFormPic.N18Click(Sender: TObject);
var
  pathname:string;
begin
  picsave.FileName:='无标题';
  if picsave.Execute then
    begin
      pathname:=picsave.FileName;
      if picsave.Filterindex=1 then
          pathname:=pathname+'.bmp';
      image1.Picture.SaveToFile(pathname);
      pathnamett:=pathname;
    end;

end;

procedure TFormPic.N17Click(Sender: TObject);
var
  pathname:string;
begin
  picsave.FileName:='无标题';
  if picsave.Execute then
    begin
      pathname:=picsave.FileName;
      if picsave.Filterindex=1 then
          pathname:=pathname+'.bmp';
      formshow.impaint.Picture.SaveToFile(pathname);
end;
end;
procedure TFormPic.ComDWChange(Sender: TObject);//***路程转换
var
  num2:string;
  t,t1:integer;
begin
     t1:=0;
     if comdw.Text  ='cm' then
        edit1.text:=inttostr(round(load*rule/rule2))+'.'+inttostr(round(load*rule/rule2*10)mod 10)
     else begin if comdw.text ='m' then
        begin
          rule3:=100;
          t1:=2;
        end;
      if comdw.Text ='km' then
        begin
          rule3:=100000;
          t1:=5;
        end;
        num2:=inttostr(round(load*rule/rule2)mod rule3);
        t:=length(num2);
        if t1<>0 then
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        edit1.text:=inttostr(round(load*rule/rule2)div rule3)+'.'+num2;
    end;
end;

procedure TFormPic.ComSChange(Sender: TObject);//***面积转换
var
  num2:string;
  t,t1:integer;
begin
     if comS.Text  ='cm^2' then
        edit2.text:=inttostr(round(S*(rule*rule/rule2/rule2)))
     else begin if comS.text ='m^2' then
        begin
          rule3:=10000;
          t1:=4;
        end;
      if comS.Text ='km^2' then
        begin
          rule3:=100000;
          t1:=10;
        end;
        if t1=10 then
        num2:=inttostr((round(s*(rule*rule/rule2/rule2))mod rule3) mod 100000)
        else num2:=inttostr(round(s*(rule*rule/rule2/rule2))mod rule3);
        t:=length(num2);
        while t<t1 do
          begin
            num2:='0'+num2;
            inc(t);
          end;
        if t1=10 then
        edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3 div 100000)+'.'+num2
        else edit2.text:=inttostr(round(s*(rule*rule/rule2/rule2))div rule3)+'.'+num2
    end;
end;


procedure TFormpic.ButtSClick(Sender: TObject);
const dot=76800;
var
 i,ssum,lines,count:longint;
 a:array[1..dot,1..2] of word;

procedure sort(k,x,y:integer); //广度标记程序   line=1:待展开 line=2:已标记 line=4:已标记线条 line=-1未标记线条:
begin
   if (linef[x,y]) then
    begin
    if line[x,y]=1 then
     begin
      line[x,y]:=2; inc(ssum);//标记已读取点,记录面积
      begin
        if (line[x+1,y]mod 2<>0) and (line[x+1,y]<>1) and (linef[x+1,y]) and (x<image1.Picture.Width-1) then
         begin inc(count); a[count,1]:=x+1; a[count,2]:=y; line[x+1,y]:=1;  end;
        if (line[x-1,y]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (x>0) then
         begin inc(count); a[count,1]:=x-1; a[count,2]:=y; line[x-1,y]:=1; end;
        if (line[x,y+1]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (x<image1.Picture.Height-1) then
         begin inc(count); a[count,2]:=y+1; a[count,1]:=x; line[x,y+1]:=1; end;
        if (line[x,y-1]mod 2<>0) and (line[x,y+1]<>1) and (linef[x,y+1]) and (y>0) then
         begin inc(count); a[count,2]:=y-1; a[count,1]:=x; line[x,y-1]:=1; end;
      end;
     end
    end
   //else begin line[x,y]:=4;  end;
  end;


procedure analyse;
 var
 r,g,b:integer;
 x,y:integer;
 begin
   for y:=0 to image1.Picture.Height-1 do
       begin
        for x:=0 to image1.Picture.Width-1 do
          begin
           r:=GETRVALUE(image1.Canvas.Pixels[x,y]);
           g:=GETGVALUE(image1.Canvas.Pixels[x,y]);
           b:=GETBVALUE(image1.Canvas.Pixels[x,y]);
           if (r<=rm) and (r>=rn) and(b<=bm) and (b>=bn) and (g>=gn) and (g<=gm) then //取点
            begin linef[x,y]:=false; inc(lines); end;
          end;
       end
 end;

begin
 fillchar(line,sizeof(line),3);
 fillchar(linef,sizeof(linef),true);
 ssum:=0;
 lines:=0;
 count:=0;
 analyse;
 fillchar(a,sizeof(a),0);
 a[1,1]:=0; a[1,2]:=0; line[0,0]:=1; count:=1;
 for i:=1 to dot do
  sort(i,a[i,1],a[i,2]);
  S:=76800-ssum+lines;
  FormPic.ComSChange(Self);
  //FormPic.Hint:='操作完成！';
  //delay(1000);
  formPic.Hint:='操作完成！请选择所需的操作方式，开始操作……'
end;

procedure TFormPic.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Formmain.Close;
end;

procedure TFormPic.N7Click(Sender: TObject);
begin
formabout.showModal;
end;

procedure TFormPic.N20Click(Sender: TObject);
begin
 if not showcol then
  begin
  showcol:=true;
  showmessage('鼠标于图片上所经点的颜色值将即时显示在状态栏上');
  N20.Caption:='取消显示颜色值';
  end
 else
 begin
   showcol:=false;
   showmessage('颜色值显示已取消！');
   N20.Caption:='显示所经点颜色值';
 end;
end;


procedure TFormPic.N21Click(Sender: TObject);
begin
  formpic.colordialog1.Color:=image1.Canvas.Pen.Color;
  if  formpic.colordialog1.Execute then
  formpic.image1.Canvas.Pen.Color:=colordialog1.Color;
end;

procedure TFormPic.N22Click(Sender: TObject);//初始化
var
  i:integer;
begin
  if pathnamett<>' ' then   formpic.Image1.Picture.LoadFromFile(pathnamett);
  ButtS.Enabled:=False;
  case application.MessageBox('请在按下“OK”后，用鼠标画出一段已知的距离，再按停止.','初始化') of
    idok:begin
           BTT:=true;
           k:=1;
           fillchar(march,sizeof(march),0);
           formpic.image1.Canvas.Pen.Color:=colordialog1.Color;
           formpic.Hint:='路线确定中……结束后请按下“停止”';
         end;
    end;
  restart:=true;
  FormPic.N21Click(self);
end;


end.

